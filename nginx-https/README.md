# Nginx

#### 过程
```
# 解压分卷
unzip nginx.zip

# 镜像导入
docker load -i nginx.zip

# 后台启动
docker-compose up -d
```

#### 访问使用
上传静态文件到html文件夹
证书放在cert文件夹中

default.conf中定义80自动跳转到443端口(https)
customize.conf可自定义编辑

浏览器访问
http://ip:80





