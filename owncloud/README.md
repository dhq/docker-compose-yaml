# 使用docker-compose搭建文件管理owncloud

#### 过程
```
# 解压分卷
cat owncloud.zip.* > owncloud.zip && unzip owncloud.zip

# 镜像导入
docker load -i owncloud.tar

# 创建数据目录
mkdir -p /data

	
# 后台启动
docker-compose up -d
```

#### 访问使用
浏览器访问
http://ip:8000





