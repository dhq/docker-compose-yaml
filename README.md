# docker-compose-yaml

#### 介绍
1. 日常docker-compose搭建环境的常见yaml
2. 自带离线docker镜像文件

例如：使用docker-compose单机快速搭建mysql,owncloud等

#### docker-compose使用

进入yaml所在文件夹操作

```
# 前台启动
docker-compose up

# 后台启动
docker-compose up -d

# 查看日志
docker-compose logs

# 停止所有服务
docker-compose stop

# 删除所有服务
docker-compose rm

# 停止并删除所有服务
docker-compose down
```


#### 镜像导入导出
```
# Linux解压zip分卷
cat owncloud.zip.* > owncloud.zip && unzip owncloud.zip

# 镜像导出
docker save -o mysql.tar docker.io/mysql:5.7.25

# 镜像导入
docker load –i mysql.tar
```


