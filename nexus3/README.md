# 使用docker-compose搭建Maven私服nexus3

#### 过程
```
# 解压分卷
cat nexus3.zip.* > nexus3.zip && unzip nexus3.zip

# 镜像导入
docker load -i nexus3.tar

# 创建数据目录
mkdir -p /data/nexus
chmod -R 777 /data/nexus

	
# 后台启动
docker-compose up -d
```

#### 访问使用
nexus初始密码
cat /data/nexus/admin.password

浏览器访问
http://ip:8081


#### Nexus3迁移过程
##### 1. 原机器上操作
##### 1.1 备份databases
1. 管理界面System-Tasks界面，点击“Create task”
2. 选择Admin-Export databases for backup
3. 填写好名称，保存路径/nexus-data，Task frequency可以选择Manual，保存之后，立即执行一次

备份文件以.bak结尾，有多个文件，会导出到宿主主机/data/nexus中

##### 1.2 备份blobs
打包整个blobs目录

##### 2. 目标机器上操作
1. 停止服务`docker-compose stop`
2. 删除/data/nexus/db中的component，config，security（对应bak文件）
3. 将`*.bak`复制到/data/nexus/restore-from-backup
4. 删除blobs,解压备份机器上的blobs到/data/nexus/
5. 启动服务`docker-compose start`







